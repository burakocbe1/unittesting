﻿using NUnit.Framework;
using System;

namespace FizzBuzz.Library.Tests
{
    [TestFixture]
    public class FizzBuzzerTests
    {

        [Test]
        public void Buzzer_WhenDefault_ReturnInput(
            [Values (1,2,4)] int input)
        {
            string output = FizzBuzzer.GetValue(input);
            Assert.AreEqual(input.ToString(), output);
        }
        [Test]
        public void Buzzer_WhenDiv3_ReturnsFizz(
            [Values(3,6)] int input)
        {
            string output = FizzBuzzer.GetValue(input);
            Assert.AreEqual("Fizz", output);
        }
        [Test]
        public void Buzzer_WhenDiv5_ReturnsBuzz(
            [Values(5, 10)] int input)
        {
            string output = FizzBuzzer.GetValue(input);
            Assert.AreEqual("Buzz", output);
        }
        [Test]
        public void Buzzer_When3And5_ReturnsFizzBuzz(
            [Values(15)] int input)
        {
            string output = FizzBuzzer.GetValue(input);
            Assert.AreEqual("FizzBuzz", output);
        }
    }
}
