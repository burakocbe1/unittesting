﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OefeningMockingOpenWeather;
using System;
using System.Collections.Generic;
using System.Text;

namespace OefeningMockingOpenWeather.Tests
{
    [TestClass()]
    public class WeatherServiceTests
    {
        [TestMethod()]
        public void GetCurrentWeatherInAntwerp_Returns_Brrr_If_Temp_Is_Below_Zero()
        {
            //Arrange
            var openWeatherMapApi = new Mock<IOpenWeatherMapApi>();
            openWeatherMapApi.Setup(x => x.GetCurrentTemperatureInAntwerp()).Returns(-1);

            //Act
            var weatherService = new WeatherService(openWeatherMapApi.Object);

            //Assert
            Assert.AreEqual("Brrrr, it's freezing", weatherService.GetCurrentWeatherInAntwerp());
        }
        [TestMethod()]
        public void GetCurrentWeatherInAntwerp_Returns_Brrr_If_Temp_Is_Below_Zero_IntegrationTest()
        {
            //Arrange
            var openWeatherMapApi = new OpenWeatherMapApi("http://localhost:3000/data/2.5/weather");

            //Act
            var weatherService = new WeatherService(openWeatherMapApi);

            //Assert
            Assert.AreEqual("Brrrr, it's freezing", weatherService.GetCurrentWeatherInAntwerp());
        }
    }
}