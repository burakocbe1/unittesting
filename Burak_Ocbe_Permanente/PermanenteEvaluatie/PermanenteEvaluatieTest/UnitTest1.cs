using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using PermanenteEvaluatie;
using PermanenteEvaluatie.Tempratuur;

namespace PermanenteEvaluatieTest
{
    [TestClass()]
    public class UnitTest1
    {
        [TestMethod()]
        public void GetTime_Returns_nacht()
        {
            //Arrange
            var ipApi = new Mock<IIpInterface>();
            ipApi.Setup(x => x.GetTime("195.130.131.38")).Returns("2020-11-21T02:23:07.680728+01:00");

            //Act
            var ipServices = new IpServices(ipApi.Object);

            //Assert
            Assert.AreEqual("Op dit IP is het momenteel nacht.", ipServices.GetTimeZone("195.130.131.38"));
        }

        [TestMethod()]
        public void GetTime_Returns_Voormiddag()
        {
            //Arrange
            var ipApi = new Mock<IIpInterface>();
            ipApi.Setup(x => x.GetTime("195.130.131.38")).Returns("2020-11-21T07:23:07.680728+01:00");

            //Act
            var ipServices = new IpServices(ipApi.Object);

            //Assert
            Assert.AreEqual("Op dit IP is het momenteel voormiddag.", ipServices.GetTimeZone("195.130.131.38"));
        }

        [TestMethod()]
        public void GetTime_Returns_Middag()
        {
            //Arrange
            var ipApi = new Mock<IIpInterface>();
            ipApi.Setup(x => x.GetTime("195.130.131.38")).Returns("2020-11-21T12:23:07.680728+01:00");

            //Act
            var ipServices = new IpServices(ipApi.Object);

            //Assert
            Assert.AreEqual("Op dit IP is het momenteel middag.", ipServices.GetTimeZone("195.130.131.38"));
        }

        [TestMethod()]
        public void GetTime_Returns_Namiddag()
        {
            //Arrange
            var ipApi = new Mock<IIpInterface>();
            ipApi.Setup(x => x.GetTime("195.130.131.38")).Returns("2020-11-21T16:23:07.680728+01:00");

            //Act
            var ipServices = new IpServices(ipApi.Object);

            //Assert
            Assert.AreEqual("Op dit IP is het momenteel namiddag.", ipServices.GetTimeZone("195.130.131.38"));
        }

        [TestMethod()]
        public void GetTime_Returns_avond()
        {
            //Arrange
            var ipApi = new Mock<IIpInterface>();
            ipApi.Setup(x => x.GetTime("195.130.131.38")).Returns("2020-11-21T18:23:07.680728+01:00");

            //Act
            var ipServices = new IpServices(ipApi.Object);

            //Assert
            Assert.AreEqual("Op dit IP is het momenteel avond.", ipServices.GetTimeZone("195.130.131.38"));
        }

        [TestMethod()]
        public void GetZoneBrussel()
        {
            //Arrange
            var ipTime = new IpTime();

            //Act
            var ipTemp = new IpTemp(ipTime);

            //Assert
            Assert.AreEqual("Brussels", ipTemp.GetZone("195.130.131.38"));
        }

        [TestMethod()]
        public void GetZoneNewYork()
        {
            //Arrange
            var ipTime = new IpTime();

            //Act
            var ipTemp = new IpTemp(ipTime);

            //Assert
            Assert.AreEqual("New_York", ipTemp.GetZone("72.229.28.185"));
        }
    }
}
