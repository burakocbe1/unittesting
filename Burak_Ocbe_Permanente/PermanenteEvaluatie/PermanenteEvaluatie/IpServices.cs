﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace PermanenteEvaluatie
{
    public class IpServices
    {
        private readonly IIpInterface ipInterface;
        public IpServices(IIpInterface ipInterface)
        {
            this.ipInterface = ipInterface;
        }

        public string GetTimeZone(string ip)
        {
            var time = ipInterface.GetTime(ip);
            var get = time.Substring(11,2);
            int test = Convert.ToInt32(get);

            if (test > 0 && test < 7)
            {
                return "Op dit IP is het momenteel nacht.";
            }
            if (test > 6 && test < 12)
            {
                return "Op dit IP is het momenteel voormiddag.";
            }
            if (test > 11 && test < 13)
            {
                return "Op dit IP is het momenteel middag.";
            }
            if (test > 12 && test < 18)
            {
                return "Op dit IP is het momenteel namiddag.";
            }
            if (test > 17)
            {
                return "Op dit IP is het momenteel avond.";
            }
            return "Fout";
        }
    }
}
