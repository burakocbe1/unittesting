﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PermanenteEvaluatie.Tempratuur;

namespace PermanenteEvaluatie
{

    class Program
    {

        static void Main(string[] args)
        {
            IpApi ip = new IpApi();
            IpServices ip2 = new IpServices(ip);


            IpTime b = new IpTime();
            IpTemp a = new IpTemp(b);

            Console.WriteLine(ip2.GetTimeZone("172.127.30.1"));
            Console.WriteLine(a.GetTemp("172.127.30.1"));
        }
    }
}
