﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace PermanenteEvaluatie.Tempratuur
{
    public class IpTemp
    {
        private readonly IpTimeZone ipTimeZone;
        public IpTemp(IpTimeZone ipTimeZone)
        {
            this.ipTimeZone = ipTimeZone;
        }

        public string GetZone(string ip)
        {
            var time = ipTimeZone.GetTime(ip);
            var location = time.Substring(time.IndexOf("/") + 1);
            return location;
        }

        public string GetTemp(string ip)
        {
            string result;
            var time = ipTimeZone.GetTime(ip);
            var location = time.Substring(time.IndexOf("/") + 1);
            string url = "http://api.openweathermap.org/data/2.5/weather?q=" + location + "&appid=b1a90ec4d94d84ecf2a3f2bb634b970d&units=metric";
            using (var httpClient = new HttpClient())
            {
                var httpRespone = httpClient.GetAsync(url).GetAwaiter().GetResult();
                var response = httpRespone.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                var json =  JsonConvert.DeserializeObject<OpenWeather>(response).main.temp.ToString();
                result = $"Op dit locatie is het momenteel {json} graden.";
            }
            return result;
        }
    }
}
