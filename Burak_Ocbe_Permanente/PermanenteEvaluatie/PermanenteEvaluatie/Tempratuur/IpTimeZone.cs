﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PermanenteEvaluatie
{
    public interface IpTimeZone
    {
        string GetTime(string ip);
    }
}
