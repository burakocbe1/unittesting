using BowlingGameScore;
using NUnit.Framework;
using System;

namespace BowlingGameScoreTests
{

    public class Tests
    {

        private BowlingGame game;
        private void RollMant(int roll, int pin)
        {
            for (int i = 0; i < roll; i++)
            {
                game.Roll(pin);
            }
        }

        [SetUp]
        public void Setup()
        {
            game = new BowlingGame();
        }

        [Test]
        public void When_Roll_GlutterGame_Returns_0()
        {
            RollMant(20, 0);
            Assert.AreEqual(0, game.Score);
        }

        [Test]
        public void When_Roll_AllOnes_Returns_20()
        {
            RollMant(20, 1);
            Assert.AreEqual(20, game.Score);
        }

        [Test]
        public void When_Roll_SpareAndThree_Return_16()
        {
            RollMant(2, 5);
            RollMant(1, 3);
            Assert.AreEqual(16, game.Score);
        }

        [Test]
        public void When_Roll_StrikeAndSeven_Return_24()
        {
            RollMant(1, 10);
            RollMant(1, 0);
            RollMant(1, 0);
            Assert.AreEqual(20, game.Score);
        }

        [Test]
        public void When_Roll_PerfectGame_Return_300()
        {
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            RollMant(1, 10);
            Assert.AreEqual(300, game.Score);
        }
    }
}