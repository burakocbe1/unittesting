﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using OefeningMockingOpenWeather;
using System;
using System.Collections.Generic;
using System.Text;

namespace OefeningMockingOpenWeather.Tests
{
    [TestClass()]
    public class WeatherServiceTests
    {
        [TestMethod()]
        public void GetCurrentWeatherInAntwerp_Returns_Brrr_If_Temp_Is_Below_Zero()
        {
            //Arrange
            var openWeatherMapApi = new Mock<IOpenWeatherMapApi>();
            openWeatherMapApi.Setup(x => x.GetCurrentTemperatureInAntwerp()).Returns(-1);

            //Act
            var weatherService = new WeatherService(openWeatherMapApi.Object);

            //Assert
            Assert.AreEqual("Brrrr, it's freezing", weatherService.GetCurrentWeatherInAntwerp());
        }
        [TestMethod()]
        public void GetCurrentWeatherInAntwerp_Returns_Cold()
        {
            //Arrange
            var openWeatherMapApi = new Mock<IOpenWeatherMapApi>();
            openWeatherMapApi.Setup(x => x.GetCurrentTemperatureInAntwerp()).Returns(2);

            //Act
            var weatherService = new WeatherService(openWeatherMapApi.Object);

            //Assert
            Assert.AreEqual("It's cold", weatherService.GetCurrentWeatherInAntwerp());
        }
        [TestMethod()]
        public void GetCurrentWeatherInAntwerp_Returns_Ok()
        {
            //Arrange
            var openWeatherMapApi = new Mock<IOpenWeatherMapApi>();
            openWeatherMapApi.Setup(x => x.GetCurrentTemperatureInAntwerp()).Returns(20);

            //Act
            var weatherService = new WeatherService(openWeatherMapApi.Object);

            //Assert
            Assert.AreEqual("it's ok", weatherService.GetCurrentWeatherInAntwerp());
        }
        [TestMethod()]
        public void GetCurrentWeatherInAntwerp_Returns_Hot()
        {
            //Arrange
            var openWeatherMapApi = new Mock<IOpenWeatherMapApi>();
            openWeatherMapApi.Setup(x => x.GetCurrentTemperatureInAntwerp()).Returns(25);

            //Act
            var weatherService = new WeatherService(openWeatherMapApi.Object);

            //Assert
            Assert.AreEqual("It's HOT!!!", weatherService.GetCurrentWeatherInAntwerp());
        }
    }
}