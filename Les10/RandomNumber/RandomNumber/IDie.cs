﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomNumber
{
    public interface IDie
    {
        public int Roll();
    }
}
