﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RandomNumber
{
    public class DieGame
    {
        private readonly IDie player1Die;
        private readonly IDie player2Die;

        public DieGame(IDie player1Die, IDie player2Die)
        {
            this.player1Die = player1Die;
            this.player2Die = player2Die;
        }

        public string Play(string player1, string player2)
        {
            var player1Roll = player1Die.Roll();
            var player2Roll = player2Die.Roll();

            if(player1Roll > player2Roll)
            {
                return $"{player1} has won";
            }
            else if(player2Roll > player1Roll)
            {
                return $"{player2} has won";
            }
            return "It`s a draw";
        }
    }
}
