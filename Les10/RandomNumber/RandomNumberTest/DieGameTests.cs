using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using RandomNumber;

namespace RandomNumberTest
{
    [TestClass]
    public class DieGameTests
    {
        [TestMethod]
        public void Play_Returns_Player1_As_Winner_If_Player1_Rolls_Highest_Value()
        {
            var p1 = new Mock<IDie>();
            p1.Setup(x => x.Roll()).Returns(6);
            var p2 = new Mock<IDie>();
            p2.Setup(x => x.Roll()).Returns(1);

            var game = new DieGame(p1.Object, p2.Object);

            Assert.AreEqual("Burak has won", game.Play("Burak","Anders"));
        }

        [TestMethod]
        public void Play_Returns_Player2_As_Winner_If_Player2_Rolls_Highest_Value()
        {
            var p1 = new Mock<IDie>();
            p1.Setup(x => x.Roll()).Returns(1);
            var p2 = new Mock<IDie>();
            p2.Setup(x => x.Roll()).Returns(6);

            var game = new DieGame(p1.Object, p2.Object);

            Assert.AreEqual("Anders has won", game.Play("Burak", "Anders"));
        }

        [TestMethod]
        public void Play_Returns_No_Winner_If_Player1_And_Player2_Rolls_Same_Value()
        {
            var p1 = new Mock<IDie>();
            p1.Setup(x => x.Roll()).Returns(1);
            var p2 = new Mock<IDie>();
            p2.Setup(x => x.Roll()).Returns(1);

            var game = new DieGame(p1.Object, p2.Object);

            Assert.AreEqual($"It`s a draw", game.Play("Burak", "Anders"));
        }

        [TestMethod]
        public void Play_Returns_No_Winner_If_Player1_And_Player2_Rolls_Same_Value_Mockoon()
        {
            var p1 = new RandomNumberApiDie("http://localhost:3000/data");
            var p2 = new RandomNumberApiDie("http://localhost:3000/data");

            var game = new DieGame(p1, p2);

            Assert.AreEqual($"It`s a draw", game.Play("Burak", "Anders"));
        }
    }
}
