﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Les5
{
    class Die: IDie
    {
        private static readonly Random Random = new Random();
        public int Roll()
        {
            return Random.Next(5) + 1;
        }
    }
}
