﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Les5
{
    public class DieMock : IDie
    {
        private int _result;
        public DieMock(int result)
        {
            _result = result;
        }
        public int Roll()
        {
            return _result;
        }
    }
}
