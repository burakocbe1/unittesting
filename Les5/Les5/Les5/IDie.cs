﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Les5
{
    public interface IDie
    {
        int Roll();
    }
}
